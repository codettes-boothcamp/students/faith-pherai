# Faith Pherai

![Picture1](img/wk1/Picture1.jpg)


# About 

I am 18 years old and I love music, dancing, learning new things, baking and capturing memories. 
Playing the guitar, singing and painting are on of my favorite pass times.
I am the only daughter and sister of a loving mother, father and awesome brother. 
I am also the aunt of a smart and very bold 2 year old girl.

# Studies

With my highschool diploma from the Mr. Dr. J. C. De Miranda Lyceum I chose to study 
something where I can be creative and get educated.
I am currently studying architecture at the Polythechnic College and learning how to code 
and be innovative at the Codettes Bootcamp 2019.

# Codettes Bootcamp 2019

Myself and fourteen other girls are helping and working with each other to show ourselves and the world that we got this. 
This all happens in an quiet space located at the IoT lab in the Etnalaan #9 
under the guidance of Mr. Theo Boomsma and Miss Julie Sundar. We have a year to educate and get educated about IoT
and digital fabrication. At the end of the year I wish to be able to make my own webpage( and anything that
I am able to think of) so I can always reference it in my line of work as an extra bit of experience and knowledge.
For our assignment at the end of the year we have to make something that has to do with us. 

# My project: LINOLA a 3D laser furniture scanner
 
 
I like decorating and redesigning my interior. One common problem that both verbs have is the placement of 
certain object in the room you are designing is sometimes hard to imagine.
Yes, there are apps that scan your room so you can sketch the furniture and adornments into a 3D softcopy model, but I think life 
can be made much easier.
I am currently studying infrastructure and we work in AutoCad. A very good website where you insert your dimentions and skecth
whatever project you are working on. You can then convert it into an 3D model and look at your mini project. 
When the task of designing the room comes it is easier to just scan your couch or go to your local building depot and let them show you
the furniture with the dimentions that fit in your room. You than get to see if the furniture you had chosen on their website is fit for your room or not.  
When looking around, these 3d furniture scanners are applications that mostly wil be used by Architects, Interior designers and Builing depots.


## The approach


LINOLA is a 3D laser furniture scanner. It can scan your furniture and room and upload them to a cloud storage. By using an AR-software and your phone camera you can place the scanned object in any part of your house to see if the sizing is correct and if it fits the aesthetic.  
If you are an interior designer this scanner makes it easier to put together a room. You can scan your own items or use the ones that are already in the galery. Just scan the room for a 3D softcopy online and then you
can start putting your furniture into the 3D softcopy model of your clients home. This gives the client a better idea of how his or her home is going 
to look like.

## Features

- Scan and make 3D softcopy models of both your furniture and room
- Drag and insert local building depots furniture into the 3D model of your clients home
- Pick and place the 3D models in your enviremont


## Trials

- Get the laser scanner to convert the real object after scanning into a 3D softcopy online
- Get the options on the app
- Make is easily accesable
- Get the stepper motor to turn
- Make the Augmented Reality based app 

[LINOLA](https://canvanizer.com/canvas/rLW0snQ18uf0H)


 People that are teaching me about digital fabrication and IoT.
I got a lot of inspiration from their own personal site

[Mister Theo Boomsma](http://fabacademy.org/2019/labs/lakazlab/students/theodorus-boomsma/)

[Miss Julie Sundar](http://fabacademy.org/2019/labs/lakazlab/students/julie-sundar/)


# Hackomation 2020

My team is called the INNOV8RS, together with Ivy amatredjo and Shofia Notoesowito we won the Hackomation 2020. The Hackomtaion theme was Sustainable Tourism and health in Suriname. My team and I made [Tribe](https://hackomation1.gitlab.io/2020/hackers/innov8rs/).

## TRIBE

![logo](img/tr/logo.jpg)

Our project provides the resort or bussines with a platform of add-ons that include a dashboard with the view of visitor density and real-time location tracking. 
And the visitor gets an mobile app with an interactive map.

Our platform has the ability for add-ons like:
*  Book
*  Pay
*  Free/busy
*  Activity calender

One of the most important things that any IOT project requires is a database to store the values, results and do some computation on them.
For our project we are using google spreadsheet to store and collect data to build the interactive mobile app on Android and Apple devices. 
The environment that we are using is glide.


### Presentation Pitch 

<iframe width="560" height="315" src="https://www.youtube.com/embed/DI8KQ_GUUbI" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

### Mobile app TRIBE

Our project can be visited with a link:[TRIBE](https://tribe-innov8rs.glideapp.io/) or with a QRcode:

![qrcode](img/tr/qrcode.jpg)

**(The walkthrough can be found on our productpage)

**Tribe Mobile Page**

![map](img/tr/map.jpg)

**Interactive map**

![map](img/tr/map+.jpg)

**Mobile app**

![start](img/tr/start.jpg)


## The INNOV8RS

![inno](img/tr/inno.png)

Contact us at: innov8rssu@gmail.com


### Ivy Amatredjo

Read about Ivy [here](https://codettes-boothcamp.gitlab.io/students/ivy-amatredjo); 
Contact: ivy.amatredjo@gmail.com 


### Shofia Notoesowito

Read about Shofia [here](https://codettes-boothcamp.gitlab.io/students/shofia-notosoewito); 
Contact: shofianoto@gmail.com 

### Faith Pherai
Read about Faith [here](https://codettes-boothcamp.gitlab.io/students/faith-pherai)
Contact: angelipherai@gmail.com

While making TRIBE we did everything together. From troubleshooting to figuring out how to make things work.
When one came with an idea we all stepped in and gave a bit of our own knowledge to help.
We are a GREAT TEAM after all!!

# Caribean Girls Hackathon 2021

![Pic1](img/wk1/Pic1.png)

My team is called The Fusion girls. We exist of:

- Devika Mahabir

- Anjessa Linger

- Faith Pherai

- Darlene Gefferie

- Yael Cairo

The theme that we chose to further investagate and make a IOT-based solution for is: **Save the ocean**

## Geartracks

GearTracks is a gear tracking and monitoring framework based on incorporating a low cost Internet of Things Solution for fisheries.
GearTracks provides users with devices called flutters and a dashboard with which they can monitor the usage, state, and position of their fishing gear. It is built in such a way to be "set and forget" and unobtrusive to the normal fisheries process.

Flutters consists of an esp32 microcontroller capable of both wifi and bluetooth. Attached to the esp32 is a pressure sensor used to detect the state of the flutter device. When the status under water is detected the esp32 will trigger the emploding of the co2 capsule causing the flutter to float to the surface.
The status is sent to the fisherman over either bluetooth or wifi.

**GearTracks Flutters**
Flutters come in two types, one for fishnets and one for crab cages.

### Presentation Pitch 

[Geartracks](https://youtu.be/UsHvvlQED3w)

<iframe width="560" height="315" src="https://youtu.be/UsHvvlQED3w" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

### Mobile app Geartracks

**Log in**

![login](img/wk1/login.jpg)

**Register**

![register](img/wk1/register.jpg)

**Profile client**

![profileclient](img/wk1/profileclient.jpg)

**QR-code scan**

![QRcodescan](img/wk1/QRcodescan.jpg)

**Owner information**

![Ownerinfo](img/wk1/Ownerinfo.jpg)

**Status**

![status](img/wk1/status.jpg)


## Fusion Girls

![fg](img/wk1/fg.jpeg)

### Devika Mahabier

Read about Devika [here](https://codettes-boothcamp.gitlab.io/students/devika-mahabier); 
Contact: sarishma.mahabir@gmail.com


### Anjessa Linger
Read about Anjessa [here](https://codettes-boothcamp.gitlab.io/students/anjessa-linger); 
Contact: anjessalinger1@gmail.com

### Faith Pherai
Read about Faith [here](https://codettes-boothcamp.gitlab.io/students/faith-pherai)
Contact: angelipherai@gmail.com

### Darlene Gefferie
Read about Darlene [here](https://codettes-boothcamp.gitlab.io/students/darlene-gefferie)
Contact: darlenegeff@hotmail.com 

### Yael Cairo
Read about Yael [here](https://codettes-boothcamp.gitlab.io/students/yael-cairo)
Contact: Yael.l.e.cairo@gmail.com
