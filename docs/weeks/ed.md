## Electronics Production and electronic design

This week we learned how to work with:

- **KiCaD:**

![kicad](../img/ep/kicad.jpg)

KiCad is a free EDA (electronic design automation) software.
It facilitates the design of schematics for electronic circuits and their conversion to PCB designs. 
It features an integrated environment for schematic capture and PCB layout design. 
Schematic capture, PCB layout, Gerber file generation/visualization, and library editing.




**FlatCam:**

1003= R10ohm

- Create New Project & Sketch KICAD

- Import Fab Library

- Import Components

- Connect Components

- Give Components correct name and value

- Annotate the design

- Assign Footprint to Symbols

- Generate Netlist

- Open Pcb New

- Read in Generate netlist

- In Pcb New Set Design Rules (Setup -Design Rules)

- Rearrange Components and Make Traces

- Place your Auxiliary Axis

Place all parameters and then save the file as an gbr-file

![6screen](../img/ep/6screen.jpg)
 
After making a folder where to save all your work. Start to make a first project.

Place various components in the workspace by selecting the components with its footprint. The footprint gives the exact dimension. Some libraries will have to be imported. My first project was an Arduino Uno. I used the Atmega328pAu as the processor. 

**Underneath is the pinout of an Atmega328p and PDIP.**

![pinout1](../img/ep/pinout1.jpg)

![pinout2](../img/ep/pinout2.png)


## KiCaD

**Heightmap**:
Makes a heightmap so the whole design is getting milled in one thickness. 

**Resonator**: gives a basic frequency for your processor or MCU
vb. 8 megahertz arduino. Check the ATmega(we used 20 megahertz)

**Capacitor**: Makes sure that everything is being filtered

**Resetswitch**: Is connected to the FTDI header (OUTPUTPIN). This resets the microcontroler.

- The resistor is 10 kohm and this is to stop the use of an open input. If you have an open input you won't know what the status is.

- The feeding is done through the FTDI

- The VCC provides the energy(5V)dit is een pull down

After placing the symbols on the workspace, place the wires to connect the symbols to complete the KiCad Sketch. Annotate the sketch so you know what is what.


![1](../img/ep/1.png)

Add the Fab mod library from the local drive and then import the library. Assign the footprint. Tools-Assign Footprints.


![1screen](../img/ep/1screen.png)


![2global](../img/ep/2global.png)


![2screen](../img/ep/2screen.png)


![3screen](../img/ep/3screen.jpg)

We generate the netlist. Before gnerating we make a special folder in the loca; project folder. All of the netlist will be saved in this folder. Click “Generate netlist” button on toolbar


**Organize the components**

![4screen](../img/ep/4screen.jpg)


**Kicad plot**

make the route trackss.

![5screen](../img/ep/5screen.jpg)


**The endresult**

After printing I soldert the components to the board. 

![7screen](../img/ep/7screen.png)


### Check

With a voltmeter you need to check if the pins are soldert properly. If they are overlapping or not soldert correctly this needs to be changed. We check the connections bij looking which pins connect to eachother in KiCaD map.





