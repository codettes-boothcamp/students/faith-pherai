# CAD and CAM

We will be looking at:

**CAD (Computer Aided Design) and use the Parametric Design**

This is the process of creating 2D or 3D models. 

**CAM (Computer Aided Manufacturing) prepare for 3D Printer, CNC & Laser production**

With CAM software and Computer controlled machinery is used to automate the printing, cnccutting and laser cutting process.


The three components that make a CAM system work are:

- Software with toolpath

- Machinery 

- Post processing that converts toolpath to gcode(a language tha machines understand)

CAD focues more on the design and relations of all of the designed components. Whereas CAM focusses more on the printing and how to get it in the real world.
After designing in CAD software this can be loaded into a CAM software. We used openscad and cura. This will now prepare the design for machining by generating a Gcode.

Openscad and Cura can be used to generate the Gcode. There are several things that need to be checked before  generating the code:
check if the design has geometry errors that can mess up the manufacturing process
create a toolpath for the design. These are coordinates that the machine will follow during the machining process
Setting machine parameters including speed, bedtemp, filament temp, cut/pierce height, etc..
Configuring nesting where the CAM system will decide the proper direction to maxamize machining effiency

After all of this is checked the Gcode is generated.

An example for a Gcode:

G01 X1 Y1 F20 T01 S500

This breaks down from left to right as:

- G01 indicates a linear move, based on coordinates X1 and Y1.

- F20 sets a feed rate, which is the distance the machine travels in one spindle revolution.

- T01 tells the machine to use Tool 1, and S500 sets the spindle speed.

**note: there is no Z1, because there is no depth*


![scad1](../img/cam/scad1.png)

```

$fn=360; // global setting
module ding(h=20, d1=15, d2=25){
    difference() {  // maakt een gat
 //union(){ // maken tot 1 geheel
        cylinder(h,d1,d2); 
        translate ([-5,-5,15])    cube([10,10,1000]);
    }
}

rotate ([0,90,0])ding();
translate ([100,0,0]) ding(25,25,15);
```

**Openscad is a software that makes 3D CAD objects using lines of script.**


The user interface of OpenSCAD has three parts:

- The viewing area: Preview and rendering output goes into the viewing area. Using the Show Axes menu entry an indicator for the coordinate axes can be enabled.

- The console window: Status information, warnings and errors are displayed in the console window.

- The text editor: The built-in text editor provides basic editing features like text search & replace and also supports syntax highlighting. There are predefined color schemes that can be selected in the Preferences dialog.

The making of a soeaker back pannel as a 3D design

```
difference(){  
    cube([200,150,9]);
    translate([20,020,0])
        cube([18,100,9]);
    //pocket
     translate([100,75,5])
        cylinder(10,12,12);
    //electronicsport
     translate([200-40,150/2,0])
        cylinder(30,30);

 }
```

## CAM for 3D printing

**CURA**

[Download Cura](https://ultimaker.com/software/ultimaker-cura)

- Install Cura

- Import STL (drag/drop)

- Resize and place parts

- Select printer

- Input printer filament material and settings

- Slice and save Gcode file to SDcard

![gcode](../img/cam/gcode.png)

### Printer

**Anycube 4Max Pro and Anycube Predator**

![3dprinter](../img/cam/3dprinter.png)

- Insert SDcard in printer

- Start printer

- Select design

- Click start

- Print and wait

**Note:The time and temperature of your filament and bed will be shown on the screen along with the level of completion.*

### Filament

There are differant types of filaments namely:

- ABS

- PLA

Check the given parameters for the respectable filament in the instruction booklet of the given machine.

### CAM - for CNC milling/cutting

![steppertable](../img/cam/steppertable.jpg)

Aspects:

- CNC Machine, Controller, 

- Material (wood/plastic/steel)

- Operations / Type (cut-out inner/outer, pocket,drilling)

- Tools/Mills/Toolchange

For each CNC/Material/Operation create table with:

- Tool type, diameter (D-mm),

- Feedrate (F - mm/min), Plunge Rate (mm/min),

- Spindle RPM (S - rpm),

- Pass-depth/step down (Z mm), Overlap if applicable (%)

- For milling on a CNC machine Import the SVG file with the 2D design from OpenScad into the CAM software. We worked online with MakerCam.

In MakerCAM we will generate the GCode, but first:

- enable flash in browser

- Right click on the page and click -> Inspect Element

- Select HTML Element: Body ->Content -> Container

- Change CSS “height:100%” to “height:100vh” )

- After that Click on File-> Open SVG file

After opening the file, put the exact parameters for the productions.For this click on each part before editing the parameters. Click on CAM and then on pocket operation. 

Change the Pocket parameters:

tool diamter(mm): 6

target depth(mm): -4

safety height(mm): 10

stock surface(mm): 0

step over(%): 40

step down(mm): 3

roughing clearness(mm): 0

feedrate(mm/minute): 1000

plunge rate (mm/minute): 500

direction: counter Clockwise

Click again on CAM and then on profile operation.

Change the Electrohole parameters:
tool diamter(mm): 6

target depth(mm): -9

inside/outside cut: inside

safety height(mm): 10

stock surface(mm): 0

step down(mm): 3

feedrate(mm/minute): 1000

plunge rate (mm/minute): 500

direction: counter Clockwise

Click again on CAM and then on profile operation.

Change the Basreflex parameters:

tool diamter(mm): 6

target depth(mm): -9

inside/outside cut: inside

safety height(mm): 10

stock surface(mm): 0

step down(mm): 10

feedrate(mm/minute): 1000

plunge rate (mm/minute): 500

direction: counter Clockwise

Click again on CAM and then on profile operation.

Change the Outside cut parameters:

tool diamter(mm): 6 

target depth(mm): -9 

inside/outside cut: Outside safety 

height(mm): 10 

stock surface(mm): 0 

step down(mm): 3 

feedrate(mm/minute): 1000 

plunge rate (mm/minute): 500 

direction: counter Clockwise

After editing all the parameters,

Click on CAM - calculate all

Check toolpath if all parts are selected

Select all toolpath

Export Gcode and name it

![ccn](../img/cam/ccn.png)
 
CNC Computer Numerical Control also Numerical control is the mechanized control of machining devices (such as drills, machines, plants and 3D printers) by implies of a computer. A CNC machine forms a chunk of fabric (metal, plastic, wood, ceramic, or composite) to meet details by taking after a coded modified instruction and without a manual administrator straightforwardly controlling the machining operation.

**Designing the CAD model**

Converting the CAD file to a CNC program

Preparing the CNC machine

Executing the machining operation

**Note:Always use a vacuum when milling a design*

### CAM - LaserWEB4 / CNCWeb

LaserWeb / CNCWeb is an application for:
generating GCODE from DXF/SVG/BITMAP/JPG/PNG files for Lasers and CNC Mills (= CAM Operations) and controlling a connected CNC / Laser machine (running one of the supported firmwares)

**Install laserWeb.**

Laser cutting could be a sort of warm partition prepare. The laser bar hits the surface of the material and warms it so emphatically that it dissolves or totally vaporizes. Once the laser bar has totally entered the material at one point, the real cutting handle starts. The laser framework takes after the selected geometry and isolates the material within the prepare. Depending on the application, the utilize of handle gasses can emphatically impact the result.

The tooldiameter for laser cutting is 0.02 mm machine. Here we need a DKF/SVG file.
Here we need a DNG/BMP file The Laser Engraving Process is a process where the laser beam physically removes the surface of the material to expose a cavity that reveals an image at eye level.

The laser creates high heat during the engraving process, which essentially causes the material to vaporize. It’s a quick process, as the material is vaporized with each pulse. This creates a cavity in the surface that is noticeable to the eye and touch. To form deeper marks with the laser engraver, repeat with several passes.
Here we need SVG/DXF file CNC operation VS Laser

CNC operations:

- Outside cut

- Inside cut

- Pocket- Drilling

- The tooldiamter here is 3-6 mm

Laser:

Cutting: Single or Multipass

Etching: mostly use for photos (PWM power:20-80%\white-Black)

Engraving: for names (S:1-255% and Q:0.01-1 % in fraction)

The tooldiameter here is 0.02 mm

Use wavelength (golflengte)

**Laser Cutting**

LaserWeb/CNCWeb for laser cutting.
As we open the LaserWeb program, we got to see the workspace/Documents/Gcode options on the Files Tab. To place a image on the workspace click on Add Document on the Documents section and select your image that you want to cut. After selecting the image, drag the image to the GCode section. Place all the right parameters after dragging the image. or you can exported a Gcode in MakerCam and Saved it as SVG file. In the LaserWeb program add the SVG file. Click on the Setings tab-> There are the options for adding the exact parameters for the Laser cut as below.

**Machine Settings Dimensions:**

Machine Weight: 600 mm Machine Height: 840 mm

Origin offset Show machine: OFF

Machine left Y: 0 mm Machine buttom X: 0 mm

**Tool Head:**

Beam: 0.2 mm

**Probe tool:**

X/Y Probe OFFset: 0 mm Z Probe OFFset: 1.7 mm

Machine Z stage: OFF Machine A stage: OFF Air assit: ON

**File Setting**

SVG: PX PER INCH: 96 BITMAP DPI: 300

**GCode Settings**

GCode Generator set on Default GCODE END: M5 TOOL ON: M10 TOOL OFF: M11 LASER INTENSITY: Q


I used Tinkercad to make my 3D design uploaded the file to cura. Their the Gcode got generated after putting in the parameters for the designated filament and printer and than printed my stepper table, raspberry camera and the enclosure for the first itteration lasers

![laser](../img/cam/laser.png)
![picam](../img/cam/picam.png)




