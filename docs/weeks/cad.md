#3D designing and 3D printing


**Assignment:**

- Practicing with the 3D Designs in TinkerCad
- Installing Fusion 360
- Installing Inkscape
- Printing the design with the 3D printer: AnyCubic for Maxpro

We ad to design our own 3D Project in TinkerCad or in Fusion 360 and then
print our own 3D Design with the AnyCubic for Maxpro.


## Autodesk TinkerCad 


![tinkercad](../img/cad/tinkercad.jpg)


Tinkercad is a free, online 3D modeling program that runs in a web browser. 
Its straightforward interface makes it esy to use. Since it got
accessible in 2011 it has ended up a prevalent stage for making models for 
3D printing as well as an entry-level presentation to helpful strong geometry in schools.
A design is made up of primitive shapes that are either solid or hole. Combining solids and 
holes together, new shapes can be created. Shapes can be imported from three formats: STL and OBJ for 3D, 
and 2-dimensional SVG shapes for extruding into 3D shapes. Tinkercad exports models in STL or OBJ formats, 
ready for 3D printing. Tinkercad also includes a feature to export 3D models to Minecraft Java Edition, 
and also offers the ability to design structures using Lego bricks.

**Apllications**

Basic operations are: 

- adding shapes

- Moving shapes

- Stretch up/down 

- Make holes

- Resize shape


## Inkscape

Inkscape is a free and open-source vector graphics editor. This software can be used to create or edit
vector graphics such as illustrations, diagrams, line arts, charts, logos, business cards, book covers,
icons, CD/DVD covers, and complex paintings. 

 


## Fusion 360

Fusion 360 is available for free personal use for individuals who are:

- Using for personal projects outside of their primary employment.
- Engaged in hobby businesses.*
- Learning for personal use, outside of a company environment or commercial training.
- Creating YouTube videos, blogs or other web content.

After downloading Fusion 360 we got to know the program by checking out all the shortcuts for drawing your own 3D Design.
it didn't differ to much  from the tinkercad but you got more options in Fusion 360. The environment was more specific, more options.

- C for Circles

- R for Rectangles

- L for a Line

- E for Extrude

- H for Hole

- M for Move

