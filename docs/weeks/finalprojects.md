# LINOLA

Bringing internet to furniture shopping

## Problem

- Home depots can not be visited because of COVID rules.

- The dimensions of the product are not always listed on the stores website.

- It's a guessing game to imagine what the furniture is going to look like in the home.

## Product

![PS](../img/fn/PS.jpg)


## Software and Hardware

### Software

**Fabscanpi 3d scanner**
 
- [Fabscan image](https://github.com/mariolukas/FabScanPi-Build-Raspbian/releases)
	
- [Software instalation](https://fabscanpi-server.readthedocs.io/en/latest/software_installation.html)
	
- Scan forRaspberry pi port (default IP is 8001) [IP scanner](https://angryip.org/download/#windows)

- Enable picamera in (work as super user ```sudo su```) ```sudo raspi-config```

- Check fab log ```cat /var/log/fabscanpi/fabscanpi.log```. This needs to be checked to know what error may be occuring and how to fix it.

- Start Fabcam ```systemctl start fabscanpi-server usermod -a -G tty ${Faith}```

![log]()
![sr]()

- Start and stop server ```/etc/init.d/fabscanpi-server start/stop```

![ws]()

- Build files can be found in ```target/build/main```

Some type of USB serial adapter may appear as /dev/ttyACM0 … (change in etc/fabscanpi/default.config.json


## Hardware

**Stepper and line laser**

On the arduino the codes for the stepper and line laser are uploaded [Fabscan firmware]().
With a cnc shield the stepper and line laser are connected to the arduino(don't forget to change the shield that you are using).


**Camera** 

The camera is connected to the raspberry pi.
