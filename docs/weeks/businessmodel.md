# Business model

Make your own business model with [canvanizer](https://canvanizer.com/)

The term business model refers to a company's plan for making a profit. It creates a visual chart with elements describing a firm or product's value proposition, infrastructure, customers ans finances. Business models are important for both new and established businesses. They help new, developing companies attract investment, recruit talent, and motivate management and staff.

We used Canvanizer to make our Business model in Canvanizer.

![bm](../img/bm/bm.png)

