# Embedded Programming

## Week 3


In week 3 we learned about:
- How electricity runs
- What the certain componants are when making a closed circuit
- How to program with an Aurdino Uno strater kit: - Basic blink
												  - Spaceship Interphase
												  - Running lights 
												  - Procedureal programming


## Arduino Uno 

The Arduino Uno is an open-source microcontroller board based on the Microchip 
ATmega328P microcontroller and developed by Arduino.cc. 
The board is equipped with sets of digital and analog input/output pins that may 
be interfaced to various expansion boards and other circuits. 

![no](../img/emp/no.jpg)


## Arduino IDE

Open-source Arduino Software (IDE) makes it easy to write code and upload it to the board.

![ar](../img/emp/ar.jpg)


### New file

**Choose the board**

*We are working with Arduino/Genuino Uno*

![9](../img/emp/9.jpg)


**The port**

*The USB cable is plugged in COM9. It also gives you an indication what board is connected.*

![8](../img/emp/8.jpg)


**Choose the example you're going to work on**

*We started with the basic blink example*

![10](../img/emp/10.jpg)


**The path**

*A new file was opened where the coding gets saved*

![es4](../img/emp/es4.jpg)


### Components

![e12](../img/emp/e12.jpg)

- **Breadboard**:
A board on which you can build electronic circuits.

- **Arduino Uno**:
The microcontroller development board. You can build 
circuits and interfaces for interactions, and telling the Arduino how to interfase the components. 

- **Resistors**:
Resist the flow of electrical energy in a circuit, changing the voltage and current as a result. The values are measured in ohms.

- **USB cable**:
This allows you to connect your Arduino Uno to your personal computor.

- **Jumper wires**:
Use these to connect components to eachother on the breadboard and to the Arduino.

- **LED's (Light Emmitting Diodes)**:
A type of diode that illuminates electricity passes through it.

- **Pushbuttons**:
Momentarily switches that are good at detecting on/off signals


*Every resistor has various colors and all these colors have different meanings as shown in the Resistance color code table here under.*

![re](../img/emp/re.jpg)


### Verify and upload

![save](../img/emp/save.jpg)

**1 = Verify:**
The code gets verified for any mistakes or wrong codes

**2 = Upload:**
The code gets uploaded to the board and you can see your coding commands come to life.


### Running lights

![blink](../img/emp/blink.jpg)

**Void setup**:
You write what you have and what you're gonna use it for.

```
void setup() {
  // initialize digital pin LED_BUILTIN as an output.
  pinMode(12, OUTPUT);
  pinMode(13, OUTPUT);
  pinMode(11, OUTPUT);
}
```


**Void loop**:
This is where you indicate the action that wil continue to loop

```
// the loop function runs over and over again forever
void loop() {
  digitalWrite(12, HIGH);   // turn the LED on (HIGH is the voltage level)
  delay(1000);                       // wait for a second
  digitalWrite(12, LOW);    // turn the LED off by making the voltage LOW
  delay(1000);
// 13 here
  digitalWrite(13, HIGH);   // turn the LED on (HIGH is the voltage level)
  delay(1000);                       // wait for a second
  digitalWrite(13, LOW);    // turn the LED off by making the voltage LOW
  delay(1000);                       // wait for a second 
 // 11 here
 digitalWrite(11, HIGH);   // turn the LED on (HIGH is the voltage level)
  delay(1000);                       // wait for a second
  digitalWrite(11, LOW);    // turn the LED off by making the voltage LOW
  delay(1000);                       // wait for a second 
}

```


### Spaceship interfase

![space](../img/emp/space.jpg)

```
int switchState = 0; // not pressed it's off 

void setup() {
  pinMode(3,OUTPUT);
  pinMode(4,OUTPUT);
  pinMode(5,OUTPUT);
  pinMode(2,INPUT);
}

void loop() {
  switchState = digitalRead(2);  // this is a command 
 // PushSwitch
 if ( switchState == LOW) {     // = means wordt // == means is gelijk aan
  //the button is not pressed



  digitalWrite(3,HIGH); // green LED
  digitalWrite(4,LOW); // red LED
  digitalWrite(5,LOW); // red LED
 }

 else {  // the button is pressed
  digitalWrite(3,LOW);
  digitalWrite(4,LOW); 
  digitalWrite(5,HIGH);
  delay(250); // wait for a quarter second
  
  // toggle the LEDs
  digitalWrite(4, HIGH);
  digitalWrite(5, LOW);
  delay(250); // wait to the beginning of the loop

  // toggle the LEDs
  digitalWrite(4, LOW);
  digitalWrite(3, HIGH);
  delay(250); // wait to the beginning of the loop
 }
} // go back to the beginning of the loop
**After subroutines running lights** 
```


### Procedural Programming with running LEDs (Subroutines)

By adding subroutines you don't have to write long
sequinces.
 
 
```
int switchState = 0; // not pressed it's off 

void setup() {
  pinMode(3,OUTPUT);
  pinMode(4,OUTPUT);
  pinMode(5,OUTPUT);
  pinMode(2,INPUT);
}

void ledsOff() {             
	//switch all leds off
	digitalwrite(3,0);
	digitalwrite(4,0);
	digitalwrite(5,0);
}

void runningLeds(){
	ledsOff();
	digitalwrite(3,1);
	delay(250);
	ledsOff();
	digitalwrite(4,1);
	delay(250);
	ledsOff();
	digitalwrite(5,1);
	delay(250);
}

// the loop function runs over and over again forever
void loop() {
  switchstate = digitalRead (2);    // this is a command  
 if (switchstate == 0){
        ledsOff();
         
  }else{
        RunningLeds();
	}
}
```


*The basic blink example executed in the picture underneath. (This is not the save way to light an LED.)*

![e11](../img/emp/e11.jpg)



## PWM (Pulse width modulation) 


```
void dimLed (int ledPin, int dutyCycle){                                        // cycle time = 100ms
    //led aan
    digitalWrite(ledPin,1);
    delay (dutyCycle);
    
    // led uit
    digitalWrite(ledPin,0);
    delay(100 - dutyCycle);
}

```

**Serial communication**

The Serial communication is the communication between The Arduino Board and your IDE on your laptop.

```
void setup() {
// setup your serial
Serial.begin(9600);
Serial.println("hello Rox");
}

void loop() {
// Send a message to your serial port/monitor
Serial.println(millis());
delay(2000);
}
```


**LDR**

![ldr](../img/emp/ldr.jpg)

The Light Dependent Resistor, A variable resistor that changes 
it's resistance based on the amount of light that falls on its face. 

```
int lightInt = 0;
int lightPc = 0;
int minL = 0;
int maxL = 1023;

// the setup routine runs once when you press reset:
void setup() {
  // initialize serial communication at 9600 bits per second:
  Serial.begin(9600);
  Serial.println ("helllo");
  pinMode(3, OUTPUT);
  pinMode(4, OUTPUT);
  pinMode(5, OUTPUT);
}

 void lightsout(){
  digitalWrite(3,0);    // turn the LED off by making the voltage LOW
  digitalWrite(4,0);    // turn the LED off by making the voltage LOW
  digitalWrite(5,0);    // turn the LED off by making the voltage LOW

  }

  void lightson(){
  lightsout();
  digitalWrite(3,1);   // turn the LED on (HIGH is the voltage level)
  delay (250);
  lightsout();
  digitalWrite(4,1);    // turn the LED off by making the voltage LOW
  delay (250);
  lightsout();
  digitalWrite(5,1);    // turn the LED off by making the voltage LOW
  delay (250);  

  }

// the loop routine runs over and over again forever:
void loop() {
  // read the input on analog pin 0:
  lightInt = analogRead(A0);
  lightPc = (lightInt - minL)*100L/(maxL-minL);
 // print out the value you read:
  Serial.println(lightPc);
  lightsout();

  // if light greater and equal to 30% led 1 on 
  // if light greater and equal to 60% led 2 on 
  // if light greater and equal to 90% led 3 on 
  if (lightPc>=30){digitalWrite(3,1);};
  if (lightPc>=60){digitalWrite(4,1);};
  if (lightPc>=90){digitalWrite(5,1);};

  if (lightPc>95){lightson();};

  delay(250);        // delay in between reads for stability
  }
```


**PWM**

To create analog signals with a digital output from 0-5v.


```
// the setup routine runs once when you press reset:
void setup() {
  
  pinMode(3, OUTPUT);
  pinMode(4, OUTPUT);
  pinMode(5, OUTPUT);
}
void dimLed (int ledPin, int dutyCycle){                     // cycle time = 100ms
    int cycleTime = 10;                                     // cycleTime in milliseconde
    //led aan
    digitalWrite(ledPin,1);
    delay (dutyCycle*cycleTime/100);
    
    // led uit
    digitalWrite(ledPin,0);
    delay(cycleTime - (dutyCycle*cycleTime/100));
  }

                         // the loop routine runs over and over again forever:
void loop() {
    dimLed(5,10);
   } 
```


**AnalogWrite**

AnalaogWrite is a name that i already in the sytem. It calculates the on and off period of a light. 
It has 255 brightness points. if you take 50% it is  
50/100 * 255 = 128
   

**PWM 1.2 Working with cycletime and dimLed**

``` 
int cycleTime = 10;   //cycletime  inms


// the setup routine runs once when you press reset:
void setup() {

  pinMode(3, OUTPUT);
  pinMode(4, OUTPUT);
  pinMode(5, OUTPUT);
}

 void dimLed(int ledPin,int dutyCycle){


   //led Aan
   digitalWrite (ledPin,1);
   delay (dutyCycle*cycleTime/100);

   //Led Uit
   digitalWrite (ledPin,0);
   delay (cycleTime - (dutyCycle*cycleTime/100));

  }

// the loop routine runs over and over again forever:
void loop() {
  dimLed(5,75);

  }
```

**PWM 1.2 Working with cycletime and dimLed with 2 LEDs**


```
int cycleTime = 10;            //cycletime  in ms
  int analogCycleTime = 0.50*255;      // Analog cycletime goes from 0 -255
 
// the setup routine runs once when you press reset:
void setup() {
  
  pinMode(3, OUTPUT);
  pinMode(4, OUTPUT);
  pinMode(5, OUTPUT);
}

 void dimLed(int ledPin,int dutyCycle){
  
 
   //led Aan
   digitalWrite (ledPin,1);
   delay (dutyCycle*cycleTime/100);

   //Led Uit
   digitalWrite (ledPin,0);
   delay (cycleTime - (dutyCycle*cycleTime/100));
  
  }

// the loop routine runs over and over again forever:
void loop() {
  dimLed(5,10);
  
  analogWrite(3,analogCycleTime);
    
  }
```


**Arduino pinout**

![pinout](../img/emp/pinout.jpg)

Pin 9,10,11,6,5,3 have a build in PWM on the Arduino Uno
PWM is used to simulate an analog signal with digital output that only goes on and offbetween 0 and 5V.
analog is everything between on and off.
by turning it on and off very quickly you simulate an analogous effect.
by varying the width of your on and off> cycletime


**DHT11**

![dht11](../img/emp/dht11.jpg)

This DFRobot DHT11 Temperature & Humidity Sensor features a temperature & humidity sensorcomplex with a calibrated digital signal output.
By using the exclusive digital-signal-acquisitiontechnique and temperature & humidity sensing technology, it ensures high reliability andexcellent long-term stability.
This sensor includes a resistive-type humidity measurementcomponent and an NTC temperature measurement component, and connects to a high-performance 8-bit microcontroller,
offering excellent quality, fast response, anti-interferenceability and cost-effectiveness.

DHT11’s power supply is 3-5.5V DC. When power is supplied to the sensor, do not send any
instruction to the sensor in within one second in order to pass the unstable status. One
capacitor valued 100nF can be added between VDD and GND for power filtering.
There are two different versions of the DHT11 you might come across.
One type has four pins, and the other type has three pins and is mounted to a small PCB.
The PCB mounted version is nice because it includes a surface mounted 10K Ohm pull up resistor for the signal line. 


**Temperature humidity set up**

```
// Example testing sketch for various DHT humidity/temperature sensors
// Written by ladyada, public domain

// REQUIRES the following Arduino libraries:
// - DHT Sensor Library: https://github.com/adafruit/DHT-sensor-library
// - Adafruit Unified Sensor Lib: https://github.com/adafruit/Adafruit_Sensor

#include "DHT.h"

#define DHTPIN 4     // Digital pin connected to the DHT sensor, look for the right pin that works for your board
// Feather HUZZAH ESP8266 note: use pins 3, 4, 5, 12, 13 or 14 --
// Pin 15 can work but DHT must be disconnected during program upload.

// Uncomment whatever type you're using!
#define DHTTYPE DHT11   // DHT 11
//#define DHTTYPE DHT22   // DHT 22  (AM2302), AM2321
//#define DHTTYPE DHT21   // DHT 21 (AM2301)

// Connect pin 1 (on the left) of the sensor to +5V
// NOTE: If using a board with 3.3V logic like an Arduino Due connect pin 1
// to 3.3V instead of 5V!
// Connect pin 2 of the sensor to whatever your DHTPIN is
// Connect pin 4 (on the right) of the sensor to GROUND
// Connect a 10K resistor from pin 2 (data) to pin 1 (power) of the sensor

// Initialize DHT sensor.
// Note that older versions of this library took an optional third parameter to
// tweak the timings for faster processors.  This parameter is no longer needed
// as the current DHT reading algorithm adjusts itself to work on faster procs.
DHT dht(DHTPIN, DHTTYPE);

void setup() {
  Serial.begin(9600);
  Serial.println(F("DHTxx test!"));

  dht.begin();
}

void loop() {
  // Wait a few seconds between measurements.
  delay(2000);

  // Reading temperature or humidity takes about 250 milliseconds!
  // Sensor readings may also be up to 2 seconds 'old' (its a very slow sensor)
  float h = dht.readHumidity();
  // Read temperature as Celsius (the default)
  float t = dht.readTemperature();
  // Read temperature as Fahrenheit (isFahrenheit = true)
  float f = dht.readTemperature(true);

  // Check if any reads failed and exit early (to try again).
  if (isnan(h) || isnan(t) || isnan(f)) {
    Serial.println(F("Failed to read from DHT sensor!"));
    return;
  }

  // Compute heat index in Fahrenheit (the default)
  float hif = dht.computeHeatIndex(f, h);
  // Compute heat index in Celsius (isFahreheit = false)
  float hic = dht.computeHeatIndex(t, h, false);

  Serial.print(F("Humidity: "));
  Serial.print(h);
  Serial.print(F("%  Temperature: "));
  Serial.print(t);
  Serial.print(F("°C "));
  Serial.print(f);
  Serial.print(F("°F  Heat index: "));
  Serial.print(hic);
  Serial.print(F("°C "));
  Serial.print(hif);
  Serial.println(F("°F"));
}
```

**H-Bridge - DC Motor - Adding the speed control**

```
const int pwm = 3 ; //initializing pin 2 as pwm
const int in_1 = 8 ;
const int in_2 = 9 ;
// Speed control
const int speedPin = A0;
int speed =0;

//For providing logic to L298 IC to choose the direction of the DC motor

void setup() {
   pinMode(pwm,OUTPUT) ; //we have to set PWM pin as output
   pinMode(in_1,OUTPUT) ; //Logic pins are also set as output
   pinMode(in_2,OUTPUT) ;
}
void loop() {
   // Detect speedPin value
   speed=analogRead(speedPin)/4;
   //For Clock wise motion , in_1 = High , in_2 = Low
   digitalWrite(in_1,HIGH) ;
   digitalWrite(in_2,LOW) ;
   analogWrite(pwm,speed) ;
   /* setting pwm of the motor to 255 we can change the speed of rotation
   by changing pwm input but we are only using arduino so we are using highest
   value to driver the motor */
   //Clockwise for 3 secs
   delay(3000) ;
   //For brake
   digitalWrite(in_1,HIGH) ;
   digitalWrite(in_2,HIGH) ;
   delay(1000) ;
   //For Anti Clock-wise motion - IN_1 = LOW , IN_2 = HIGH
   digitalWrite(in_1,LOW) ;
   digitalWrite(in_2,HIGH) ;
   delay(3000) ;
   //For brake
   digitalWrite(in_1,HIGH) ;
   digitalWrite(in_2,HIGH) ;
   delay(1000) ;
}
```


**H-Bridge - DC Motor - speed & direction control**

![motor control](../img/emp/motor control.jpg)

A potentiometer is a type of voltage divider. When the knob is turned, 
the voltage between the middle- and power pin is changed.
So I can read this change on an analog input. I have connect the middle 
pin to the analog pin ```A0```. This will control the position of my Servo motor.

The **map function** is intended to change one range of values into another range 
of values and a common use is to read an analogue input (10 bits long, so values 
range from 0 to 1023) and change the output to a byte so the output would be 
from 0 to 255.

```
const int pwm = 3 ; //initializing pin 2 as pwm
const int in_1 = 8 ;
const int in_2 = 9 ;
//For providing logic to L298 IC to choose the direction of the DC motor

void setup() {
   pinMode(pwm,OUTPUT) ; //we have to set PWM pin as output
   pinMode(in_1,OUTPUT) ; //Logic pins are also set as output
   pinMode(in_2,OUTPUT) ;
   pinMode(3,OUTPUT);
   Serial.begin(9600);
  
}


void loop() {
  int duty = analogRead(A0)-512/2;
  Serial.println(duty);
  analogWrite (pwm,abs(duty));
  
  if (duty>0){
    // turn CW
  digitalWrite(in_1, LOW);
  digitalWrite(in_2, HIGH);
  }
  
  if(duty<0){
    // turn CW
  digitalWrite(in_1, HIGH);
  digitalWrite(in_2, LOW);
  }
  
  if(duty==0){  
    // BRAKE
  digitalWrite(in_1, HIGH);
  digitalWrite(in_2, HIGH);
  }
}
```


**H-Bridge - DC Motor - speed & direction control(using Mapping function)**

```
// Declare ur variables
const int pwm = 3;
Const int jogPin =A0; // connects the pot-meter
const int in_1 = 8;
const int in_2 = 9;
Const int deadZone-5;   //after starts repond
Int duty=0;         // duty cycle for PWM


void setup(){
    pinMode(in_1, OUTPUT);  
    pinMode(in_2, OUTPUT);
    pinMode(3, OUTPUT);
    Serial.begin(9600);
}

void loop(){
 duty = map(analogRead(jogPin),0,1023,-255,255);
    Serial.println(duty);
    analogWrite(pwm,abs(duty)); // abs duty to PWM
    if(duty> 0 - deadZone){
        // turn CW
      digitalWrite(in_1,LOW);
      digitalWrite(in_2,HIGH); 
    }
    if(duty < 0 - deadZone){
        // turn CCW
      digitalWrite(in_1,HIGH);
      digitalWrite(in_2,LOW); 
    }
    if(abs(duty) <= deadZone){
        // BRAKE
      digitalWrite(in_1,HIGH);
      digitalWrite(in_2,HIGH); 
    }  
}
```


**Servo Motor**

The servo motor has three wires coming out of it. 
One is power(red) and one is ground( ), and the third is the control line that 
will receive information from the Arduino. the shaft on top of the motor 
can be moved bij making the voltage higher or lower. When a servo motor starts to 
move, it draws more current than if it were already in motion. This will 
cause a dip in the voltage on your board. By placing a 100uf capacitor 
across power and ground right next to the male header you can smooth out 
any voltage changes that may occur. You can place a capacitor across 
the power and ground going into the potentiometer. These are called decoupling 
capacitors because they reduce or decouple, changes caused by the components from 
the rest of the circuit. Connecting the cathode to ground and anode to power.
If the capacitors are put in backwards, they can explode!

![servo motor](../img/emp/servo motor.jpg)

```
#include <Servo.h>
Servo myServo ;
int const potPin = A0 ;
int potVal;
int angle; // hoek van 180 graden


void setup()
{
  myServo. attach(9);
  Serial.begin(9600); 
  
}


void loop()
{
  potVal = analogRead(potPin);
  Serial.print("potVal:");
  Serial.print(potVal);
  
  angle= map(potVal,0,1023,0,179);
  Serial.print(",angle:");
  Serial.println(angle);
  //analogWrite(3,map(potval,0,1023,0,179));
//myServo heeft al eentje ingebouwd.
  myServo.write(angle);
  delay(15);
  
}
```


**Stepper Motor**

Stepper motors are motors that have multiple coils in them, so that they can be moved in 
small increments or steps. Stepper motors are typically either unipolar or bipolar, meaning 
that they have either one main power connection or two. Whether a stepper is unipolar or 
bipolar,it can be controlled with a H-bridge. There is no stepper motor on tinkercad, so 
we are using 3 led light to see the blinking steps. Build the circuit: 

- Connect power and ground on the breadboard to power and ground from the microcontroller. Use 5V and a ground on th Arduino. 

- Place a L293D H-bridge on the breadboard. Stepper motors has 2 coils, so it is like driving to motors with the H-bridge. 

- Connect pin 8 to input 1 

- Connect pin 9 to input 2 

- Connect pin 10 to input 4 

- Connect pin 11 to input 3 

- Connect 4 leds to output 1,2,3,4 

- Use 220 ohm resistor for all the leds 

- Use a potentiometer and connect the wiper pin to A0

When a servomotor starts to move, it draws more current than if it were already in motion. By placing a 100uF capacitor across ground, 
it smoothes out any voltage dip that might occur on the breadboard.

![steppermotor](../img/emp/steppermotor.jpg)

```
#include <Stepper.h>

const int stepsPerRevolution = 200;  // change this to fit the number of steps per revolution
                                     // for your motor

// initialize the stepper library on pins 8 through 11:
Stepper myStepper(stepsPerRevolution, 8,9,10,11);

int stepcount=0; //the number of steps the motor has taken

void setup() {
  // initialize the serial port:
  Serial.begin(9600);
}

void loop() {
  Serial.print("steps:");
  Serial.printIn(stepCount);

  int stepDel= map(analogread(A0),1023,-500,500);
  if(stepDel >5){
  myStepper.step(1);
  stepCount++; //stepCount=stepCount+1
  }

  if(stepDel >-5){
  myStepper.step(-1)
  stepCount++; //stepCount=stepCount-1
  
```

***For most of the circuits Autodesk Tinkercad was used. This is an online program that runs on your webbrowser.***
***It can be used to create and learn about circuits, 3d designs, codebloks and lessons.***