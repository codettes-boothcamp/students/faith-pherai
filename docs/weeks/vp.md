# Video Production

## Week 2

We had a guest over, Damian Naipal(Cameraman, Editor at Supreme TV
).He thought us the basics about Adobe Premier Pro CC 2019.


### Create a new project

File > New > Project

![np](../img/vp/np.jpg)


### Name your project

Name >

Capture > HDV

![n11](../img/vp/n11.jpg)


### Import your file

File > import or drag video from document

![m](../img/vp/m.jpg)


### Workspace

WorkSpace → Project / Timeline / Effects

Drag video (left )to timeline (right)

![e](../img/vp/e.jpg)


### Change the background

Effects > Ultra Key

Effects Controls > Key Color (remove background)

Import > Background Image > add between audio and video 

**Background small**: Right click > Scale to size

![n2](../img/vp/n2.jpg)


### Add a tekst

Select Tool > Text

![tekstn3](../img/vp/tekstn3.jpg)


### Use Audio gain to change the volume of the video


![gainn4](../img/vp/gainn4.jpg)

![gainn5](../img/vp/gainn5.jpg)


### Video effects


![hmn8](../img/vp/hmn8.jpg)


### Export and save your video

Export > Media

![exportn6](../img/vp/exportn6.jpg)

Youtube > H.264

Preset > Youtube 720p HD

Output > Select Path

![exportn7](../img/vp/exportn7.jpg)

### Workspace

WorkSpace > Project / Timeline / Effects

Drag video (left )to timeline (right)

![e](../img/vp/e.jpg)

