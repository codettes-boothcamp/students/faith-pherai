# Project Managment

## Week 1


![pm](../img/pm/pm.jpg)


In week 1 of the innovative bootcamp: 

- Downlaoded GitHub desktop
- Sign up for GitHub desktop
- Sign up for Gitlab
- I installed GitHub desktop and Notepad++ on my laptop. 
- In Gitlab I build my personal site describing myself and my final project. 
- I learned to build my personal site using MkDocs. 
  MkDocs is a static site generator that is used for project documentation. 
It generates html sites that you can host on Github pages or anywhere you choose.  
- To be able to make changes locally and push them online, I cloned my online Gitlab repository in GitHub 
  and through Notepad++ I can make changes and then push them through GitHub to Gitlab ( Git tutorial).
  

## Local file system

![file](../img/pm/file.jpg)


## Clone your repository


### Gitlab clone

![gclone](../img/pm/gclone.jpg)                                      


### Github desktop clone

![clone](../img/pm/clone.jpg)      

To clone your Gitlab repository the next steps should be taken:
 
- In Gitlab your site's url will be shown to you. 
- In GitHub ask to clone a repository so you can work on it.
- Copy your url and paste it in GitHub. (just like in the picture)
  your local path will also be made visible.
  
  
## Notepad++ 

*Change made*

![1 notepad](../img/pm/1 notepad.jpg)

Notepad++ is a application where you can edit text for your site. Using Notepad++ you can locally edit your site, 
add pictures, links, etc. and push them to your Gitlab.

## GitHub desktop 

*Auto detection and commit*

![2 github](../img/pm/2 github.jpg)


After downlaoding and registering for your GitHub desktop you can clone your Gitlab repository. 
After cloning your repository you can push and commit your edit from Notepad++ to Gitlab

## Gitlab 

*Change detected*

![3 online](../img/pm/3 online.jpg)


After registering for an Gitlab account you can choose your type of 
template you want to work on. You can edit it either via Notepad++ and push or online.


[Faith Pherai](https://codettes-boothcamp.gitlab.io/students/faith-pherai)

