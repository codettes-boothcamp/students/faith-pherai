## Week 8 



## Raspberry pi 3

Raspberry pi is a microcomputer that can be used in various projects. The pi is an essential 
hardware on which you can instal the needed software to controle electronics and explore the Internet of Things.
It runs a **Linux-based** operating system. 


**Raspberry pi 3 Model B specification**


**CPU**: Quad Core 1.2GHz Broadcom BCM2837 64bit CPU, 1GB RAM.

**Network**: BCM43438 wireless LAN and Bluetooth Low Energy (BLE) on board, 100 Base Ethernet.

**Ports**: 40-pin extended GPIO, 4 USB 2 ports, 4 Pole stereo output and composite video port, HDMI, CSI camera port, DSI display port,
Micro SD port for loading your operating system and storing data.

**Power**: Micro USB power source up to 2.5A.


## Set-up

**Components and software to start with the Raspberry pi 3 you need:**

- Instal [WinDisk32Imager]() (this is to format your SD card)

- Instal Raspberry pi image [Raspbian OS]()

- Instal putty

- Instal Winscp

- Instal Notepad++

- SD card with a minimal of 16gb of storage

- Micro USB (this will be the powersource)

- Straight cable (Ethernet cable directly from the source) or Cross cable (ethernet cable from your laptop)


**Setting up Raspberry Pi 3**

1. Write the Raspbian OS on your SD card using Win32DiskImager

2. Open *cmdline.txt* and hardcode the adress of your Raspberry Pi 3 (ip=192.168.1.xxx).If you don't do 
this you can't use putty because it won't be found.

3. In your sdcard you add a ssh file without an extension (*.txt*).This so putty can access the Raspberry Pi 3 remotely.

4. Configure pc ethernet adapter config to match the RPi subnet but different adress.
pictures 

5. Insert SD card into Raspberry Pi

6. Connect Pi with pc using cross cable (at home you use a direct cable to the internet via a switch or hub)

7. Power up the Pi

8. Ping (IP) 

9. Open Putty and enter IP

10. Enter username and password (pi/'raspberry')

11. Configure Raspbian.Run Rpi setup wizard  type  > raspi-config

12. Enable root user (command: sudo su (super user))

13. Install Nodejs & Npm 

14. Nodejs folder structure

15. Install Python & PiP 

16. Connect Winscp to Rpi

### Ethernet Setup

In controle pannel > Network and internet > Network and sharing center > Change adapter settings > Ethernet (right click). 

***Some of the Basic Linus Commands are:***

cd : to go to a directory

cd ..: to go back to your last directory

touch: to create or update a file

mkdir: to cfreate a Directory

ls: To see whats in your directory (folder)

rm: To remove a file

rmdir: To remove a folder

With these commands we can config the raspberryPi headless, that means that we can install files and make folders without using a keyboard or a monitor.

Nodejs Folder Structure / Frames

- nodejs
- projects
- project1:
		* index.js
- public
		* index.html
		* css
- * + style.css
- package.json 

Nodejs and Python are two different website servers. To check which version our raspberry pi
is we use:

```
Nodejs__version
```

```
python__version
```

```
pip__version
```

We use NPM to download the nodejs dependency(library). For python we use ```pip v. 1.5.6 Express```
Flask is the app for python.

**Putty**

PuTTY is open source software that is available with source code and is developed and supported by a group of volunteers.
Download putty [here](https://www.chiark.greenend.org.uk/~sgtatham/putty/latest.html) 


## Javascript

Javascript is a scripting language for the web. You can build entire websites with javascript. 

Open putty and type in the following commands:

- node --version (to see the version of nodejs first)
- curl -sL https://deb.nodesource.com/setup_8.x | sudo -E bash - sudo apt-get install nodejs (to install the right version of nodejs)
- index.js open it and edit it
/ index.js

```
/**
 * Required External Modules
 */

/**
 * App Variables
 */

/**
 *  App Configuration
 */

/**
 * Routes Definitions
 */

/**
 * Server Activation
 */

```

In the project1 folder:

- Type npm space init space -y
- npm i D nodemon (to install nodemon)
- integrate npm init -y
- you should have your structure in your index.js after you did that you have to put code in it
- npm i express (i means install)
- open package.json and change "test..." to " "dev": "nodemon ./index.js"

All packages have a package.json file. This houses various metadata that is neede for the project.
It is used to give information to npm that enables it to e=identify the project and the project's dependencies.
The package.json is mostly located at the root directory of a Node.js project.

Edit index.js

```
// index.js

/**
 * Required External Modules
 */

const express = require("express");
const path = require("path");

/**
 * App Variables
 */

const app = express();
const port = process.env.PORT ||"8000";

/**
 *  App Configuration
 */


app.use(express.static(path.join(__dirname, "public")));

/**
 * Routes Definitions
 */



/**
 * Server Activation
 */

app.listen(port, () => {
  console.log(`Listening to requests on http://localhost:${port}`);
});
```
In the index.html file add in these basic webpage html-code:

```
<
html>
<body>
<h1>My website</h1>
</body>
</html>
```
To start the server click *npm run dev* in putty and in your browser add *http://192.168.1.166:8000*.
The 8000 is the webpage port.

**API**

a set of functions and procedures allowing the creation of applications that access the features or data of an operating system, application, or other service.

**jquery**

jQuery is a fast, small, and feature-rich JavaScript library. It makes things like HTML document traversal and manipulation, event handling, animation, and Ajax much simpler with an easy-to-use API that works across a multitude of browsers.

**ajax calls**

An Ajax call is an asynchronous request initiated by the browser that does not directly result in a page transition.

## Variables

A variable is any factor, trait, or condition that can exist in differing amounts or types. An experiment usually has three kinds of variables: independent, dependent, and controlled.

### Javascript ```<script>```

In de ```<body>``` we put the script before closing the tag.
We added a chart and to change the name of one of the boxes we used "removedata" and it is being read from there by javascript.


```

<script> 
document.getElementById("removeData").innerHTML = "My first javapage";
</script>

```


**getElement**

[getElement](http://xahlee.info/js/js_get_elements.html)


**Javascript statement**

[Javascript statement](http://w3schools.com/js/tryit.asp?filename=tryjs_statement)


```

<!DOCTYPE html>
<html>
<body>

<h2>JavaScript Statements</h2>

<p>A <b>JavaScript shofi</b> is a list of <b>statements</b> to be executed by a computer.</p>

<p id="demo"></p>

<script>
var x, y, z;  // Statement 1
x = 2;    // Statement 2
y = 6;    // Statement 3
z = x + y;  // Statement 4

document.getElementById("demo").innerHTML =
"The value of z is " + z + ".";  
</script>

</body>
</html>

```


## Operators


**Javaoperators**

```

<!DOCTYPE html>
<html>
<body>

<h2>JavaScript Operators</h2>

<p>x = 5, y = 2, calculate z = x + y, and display z:</p>

<p id="demo"></p>

<script>
var x = 5;
var y = 2;
var z = x + y;
document.getElementById("demo").innerHTML = z;
</script>

</body>
</html>

```


|  Operator |  Description  |   
| --------  |:-------------:|
| +  		| right-aligned |     
| -		    | Addition	    |       
| *         | Subtraction   |   
| **        | Exponentiation| 
| /         | Division      | 
| %         | Modulus       | 
| ++        | Increment     | 
| --        | Decrement     | 

```<BR>``` is nodig voor regel overslaan


**console.log(z)**

To run information to see if the background info went or not.



**config.data.dataset**


## Functions

**Javascript functions**

```

document.getElementById("main").innerHTML += " <BR> het antwoord is : " + maal(y,x);

function maal(pinkeltje1, pinkeltje2) {
			return pinkeltje1 * pinkeltje2;   // The function returns the product of p1 and p2
		}
```


## Events

**javascript events**

*button*

```

<button onclick="this.innerHTML = Date()">The time is?</button>

```


**listener**

```

document.getElementById('buttonDate').addEventListener('click',function() {
	document.getElementById(main).innerHTML = "op deze tijd" + Date() + "<BR> het antwoord is : " + maal(y,x);
		});
		
```


## Arrays ```[]```

- shift
- push
- pop

```
var mygifts[];
mygifts = ["car","boat","BFF"] //[0,1,2] altijf beginnen met 0
mygifts(2);
```


## Objects  ```{}```

Value pairs. dus beschrijving en waarde
```
var myobj{};
```

**MQTT**

```
(codetts/dev1/temp/="30"
codetts/dev2/garage/="open"
"mqtt":"/roxannehome/garage/lamp=on")
```

**NPM** means node package manager

we made nodejs with a socket server. by npm instal we put the socket.io. the socket is used for a chat to connect with other users.
we changed our index.js, package,js,index.html with one to make an online chat. libraries koppelen. 

$ /jquery/getelementbyid

# getelemntbyid / getelementbyname

This returns an Element object representing the element whose id property matches the specified string.

```
sudo apt-get install mosquitto instal in raspberry pi
```
